import React, { Component } from 'react';
import Navbar from './Components/NavBar'
import { BrowserRouter, Route } from 'react-router-dom'
import Home from './Components/Home'
import Login from './Components/Login'
import Signup from './Components/Signup'
import SearchIngridient from './Components/SearchIngridient'
import SearchReciepe from './Components/SearchReciepe'
import ScanReciept from './Components/ScanReciept'
import MyIngridients from './Components/MyIngridients'
import ReciepContent from './Components/ReciepContent'
import FavouriteRecipes from './Components/FavouriteRecipes'
import './App.css';
import ProtectedRoute from './Components/ProtectedRoute'

class App extends Component {

    constructor(props){
      super(props)
      this.state = {
        user: null,
        isLoggedIn: false
      }
      this.onUserSignup = this.onUserSignup.bind(this);
      this.onLogin = this.onLogin.bind(this);
      this.onLogout = this.onLogout.bind(this);
      this.addIngridientToUser = this.addIngridientToUser.bind(this);
      this.removeIngridientFromUser = this.removeIngridientFromUser.bind(this);
      this.fetchUpdateUser = this.fetchUpdateUser.bind(this);
      this.handleFavouriteRecipe = this.handleFavouriteRecipe.bind(this);
      this.removeFavouriteRecipe = this.removeFavouriteRecipe.bind(this);
      this.checkForFavouriteRecipe = this.checkForFavouriteRecipe.bind(this);
      this.updateUser = this.updateUser.bind(this);
  }

  onUserSignup(user) {
    this.setState({
      user: user,
      isLoggedIn: true
    })

    // this.fetchRecommendRecipes((recommendedRecipes) => {
    //   this.setState({
    //     recommendedRecipes: recommendedRecipes
    //   })
    // })

  }

  onLogout(){
    this.setState({
      user: null,
      isLoggedIn:false
    })
  }

  onLogin(user){
    this.setState({
      user: user,
      isLoggedIn: true
    })
  }

  

  handleFavouriteRecipe(recipe){
    this.state.user.favouriteRecipes.push(recipe)

    this.fetchUpdateUser();
  }

  addIngridientToUser(Ingridient){
    this.state.user.ingridients.push(Ingridient);

    this.fetchUpdateUser();
    this.forceUpdate();
  }

  removeIngridientFromUser(ingridient){
    console.log(this.state.user.ingridients)
    for(let i = 0 ; i < this.state.user.ingridients.length ; i++){
      if(ingridient.id === this.state.user.ingridients[i].id){
        this.state.user.ingridients.splice(i, 1);
      }
    }
    console.log(this.state.user.ingridients)

    this.fetchUpdateUser();
    this.forceUpdate();
  }

  removeFavouriteRecipe(recipe){
    console.log(this.state.user.favouriteRecipes)
    for(let i = 0 ; i < this.state.user.favouriteRecipes.length ; i++){
      if(recipe.id === this.state.user.favouriteRecipes[i].id){
        this.state.user.favouriteRecipes.splice(i, 1);
      }
    }
    console.log(this.state.user.favouriteRecipes)

    this.fetchUpdateUser((user) => {
      this.setState({
        user: user
      })
    })
  }

  updateUser(user){
    this.setState({
      user: user
    })
  }

  fetchUpdateUser(callback){
    let url = 'http://localhost:8080/recipeforu/users/update';

    fetch(url + "/" + this.state.user.email, {
      method: 'PUT',
      body: JSON.stringify(this.state.user),
      headers:{
        'Content-Type': 'application/json'
      }
      }).then(response => {return response.json();})
        .catch(error => console.error('Error:', error));
  }

  checkForFavouriteRecipe(recipe){
    for(let i = 0 ; i < this.state.user.favouriteRecipes.length ; i++){
      if(recipe.id === this.state.user.favouriteRecipes[i].id){
        return true;
      }
    }

    return false;
  }


  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <img id="home-image" className="responsive-img" alt="kitchen" src={require('./kitchen-banner.jpg')}></img>
          <Navbar user={this.state.user} onLogout={this.onLogout}/>
          <Route exact path='/' render={(props) => <Home user={this.state.user} {...props}/>} />
          <Route path='/recipe/:recipe_id'  render={(props) => <ReciepContent user={this.state.user} {...props} />} />
          <Route path='/login' render={(props) => <Login onLogin={this.onLogin} {...props}/>} />
          <Route path='/signup' render={(props) => <Signup onUserSignup={this.onUserSignup} {...props}/>} />
          <Route path='/searchIngridient' render={(props) => <SearchIngridient loggedIn={this.state.isLoggedIn} addIngridientToUser={this.addIngridientToUser} removeIngridientFromUser={this.removeIngridientFromUser} userIngridients={this.state.user.ingridients} {...props}/>} />
          <Route path='/searchRecipe' render={(props) => <SearchReciepe recommendUserId={this.state.user.recommendUserId} userName={this.state.user.userName} recommendedRecipes={this.state.recommendedRecipes} loggedIn={this.state.isLoggedIn} handleFavouriteRecipe={this.handleFavouriteRecipe} checkForFavouriteRecipe={this.checkForFavouriteRecipe} removeFavouriteRecipe={this.removeFavouriteRecipe} userIngridients={this.state.user.ingridients} user={this.state.user} {...props}/>} />
          <Route path='/scanReciept' render={(props) => <ScanReciept loggedIn={this.state.isLoggedIn} user={this.state.user} updateUser={this.updateUser} {...props}/>} />
          <Route path='/FavouriteRecipes' render={(props) => <FavouriteRecipes loggedIn={this.state.isLoggedIn} recieps={this.state.user.favouriteRecipes} removeFavouriteRecipe={this.removeFavouriteRecipe} {...props}/>} />
          {this.state.user
          ?
          <Route path='/MyIngridients' render={(props) => <MyIngridients loggedIn={this.state.isLoggedIn} ingridients={this.state.user.ingridients} removeIngridientFromUser={this.removeIngridientFromUser} {...props}/>} />
          :
          null
          }
          
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
