import React, { Component } from "react";
import './SearchIngridientByCatagory.css';
import Collapsible from 'react-collapsible';
import IngridientItem from './IngridientItem';

class SearchIngridientByCatagory extends Component {
    constructor(props){
        super(props);
        this.state = {
            ingridientsDiary: [],
            ingridientsVeg: [],
            ingridientsFruit: [],
            ingridientsSpice: [],
            ingridientsMeat: [],
            ingridientsSauce: []

        }  
    }

    componentDidMount(){
        this.fetchSearchIngridientResult((ingridients) => {
            for(let i = 0 ; i < ingridients.length ; i++){
                console.log(ingridients[i].type)
                if(ingridients[i].type == "DAIRY"){
                    this.state.ingridientsDiary.push(ingridients[i])
                } else if(ingridients[i].type == "VEGETABLE"){
                    this.state.ingridientsVeg.push(ingridients[i])
                } else if(ingridients[i].type == "FRUITS"){
                    this.state.ingridientsFruit.push(ingridients[i])
                } else if(ingridients[i].type == "SPICE"){
                    this.state.ingridientsSpice.push(ingridients[i])
                } else if(ingridients[i].type == "MEAT"){
                    this.state.ingridientsMeat.push(ingridients[i])
                } else {
                    this.state.ingridientsSauce.push(ingridients[i])
                }
            }
            this.forceUpdate();
          })
    }

    fetchSearchIngridientResult(callback){
        let url = 'http://localhost:8080/recipeforu/ingridients';
        
        fetch(url, {
          method: 'GET',
          headers:{
            'Content-Type': 'application/json'
        }
        }).then(response => {return response.json();})
          .then(response => {return callback(response)})
          .catch(error => console.error('Error:', error));
    }

        render() {
            let counter = 0;
            return (
                    <div className="">
                     
                        <div className="category-item text-center">
                            <Collapsible trigger={<span><img src={require('../Dairy.png')} alt="milk" width="48" height="48" /> <br /> Diary</span>}>
                                <div className="row">
                                    <ul className='ingridients-list'>
                                        { this.state.ingridientsDiary && this.state.ingridientsDiary.length !== 0
                                        ?
                                        this.state.ingridientsDiary.map(ingridient => {
                                                    if(counter % 4 == 0){
                                                        counter++;
                                                        return <span className="col s4"><li key={ingridient.id} ><IngridientItem userIngridients={this.props.userIngridients} addIngridientToUser={ this.props.addIngridientToUser } removeIngridientFromUser= {this.props.removeIngridientFromUser} ingridient={ ingridient }/></li></span>;
                                                    } else {
                                                        counter++;
                                                        return <span className="col s4"><li key={ingridient.id} ><IngridientItem userIngridients={this.props.userIngridients} addIngridientToUser={ this.props.addIngridientToUser } removeIngridientFromUser= {this.props.removeIngridientFromUser} ingridient={ ingridient }/></li></span>  ;
                                                    }
                                                })
                                        :
                                        null
                                        }
                                    </ul>
                                </div>  
                            </Collapsible>
   
					    </div>

                        <div className="category-item text-center">
                            <Collapsible trigger={<span><img src={require('../Vegetables.png')} alt="milk" width="48" height="48" /><br /> Vegetables</span>}>
                                <div className="row">
                                    <ul className='ingridients-list'>
                                        { this.state.ingridientsVeg && this.state.ingridientsVeg.length !== 0
                                        ?
                                        this.state.ingridientsVeg.map(ingridient => {
                                                    if(counter % 4 == 0){
                                                        counter++;
                                                        return <span className="col s4"><li key={ingridient.id} ><IngridientItem userIngridients={this.props.userIngridients} addIngridientToUser={ this.props.addIngridientToUser } removeIngridientFromUser= {this.props.removeIngridientFromUser} ingridient={ ingridient }/></li></span>;
                                                    } else {
                                                        counter++;
                                                        return <span className="col s4"><li key={ingridient.id} ><IngridientItem userIngridients={this.props.userIngridients} addIngridientToUser={ this.props.addIngridientToUser } removeIngridientFromUser= {this.props.removeIngridientFromUser} ingridient={ ingridient }/></li></span>  ;
                                                    }
                                                })
                                        :
                                        null
                                        }
                                    </ul>
                                </div>  
                            </Collapsible>
   
					    </div>

                        <div className="category-item text-center">
                            <Collapsible trigger={<span><img src={require('../Fruits.png')} alt="milk" width="48" height="48" /> <br /> Fruits</span>}>
                                <div className="row">
                                    <ul className='ingridients-list'>
                                        { this.state.ingridientsFruit && this.state.ingridientsFruit.length !== 0
                                        ?
                                        this.state.ingridientsFruit.map(ingridient => {
                                                    if(counter % 4 == 0){
                                                        counter++;
                                                        return <span className="col s4"><li key={ingridient.id} ><IngridientItem userIngridients={this.props.userIngridients} addIngridientToUser={ this.props.addIngridientToUser } removeIngridientFromUser= {this.props.removeIngridientFromUser} ingridient={ ingridient }/></li></span>;
                                                    } else {
                                                        counter++;
                                                        return <span className="col s4"><li key={ingridient.id} ><IngridientItem userIngridients={this.props.userIngridients} addIngridientToUser={ this.props.addIngridientToUser } removeIngridientFromUser= {this.props.removeIngridientFromUser} ingridient={ ingridient }/></li></span>  ;
                                                    }
                                                })
                                        :
                                        null
                                        }
                                    </ul>
                                </div>  
                            </Collapsible>
   
					    </div>

                        <div className="category-item text-center">
                            <Collapsible trigger={<span><img src={require('../Meats.png')} alt="milk" width="48" height="48" /><br /> Meats</span>}>
                                <div className="row">
                                    <ul className='ingridients-list'>
                                        { this.state.ingridientsMeat && this.state.ingridientsMeat.length !== 0
                                        ?
                                        this.state.ingridientsMeat.map(ingridient => {
                                                    if(counter % 4 == 0){
                                                        counter++;
                                                        return <span className="col s4"><li key={ingridient.id} ><IngridientItem userIngridients={this.props.userIngridients} addIngridientToUser={ this.props.addIngridientToUser } removeIngridientFromUser= {this.props.removeIngridientFromUser} ingridient={ ingridient }/></li></span>;
                                                    } else {
                                                        counter++;
                                                        return <span className="col s4"><li key={ingridient.id} ><IngridientItem userIngridients={this.props.userIngridients} addIngridientToUser={ this.props.addIngridientToUser } removeIngridientFromUser= {this.props.removeIngridientFromUser} ingridient={ ingridient }/></li></span>  ;
                                                    }
                                                })
                                        :
                                        null
                                        }
                                    </ul>
                                </div>  
                            </Collapsible>
   
					    </div>

                        <div className="category-item text-center">
                            <Collapsible trigger={<span><img src={require('../Spices.png')} alt="milk" width="48" height="48" /><br /> Spices</span>}>
                                <div className="row">
                                    <ul className='ingridients-list'>
                                        { this.state.ingridientsSpice && this.state.ingridientsSpice.length !== 0
                                        ?
                                        this.state.ingridientsSpice.map(ingridient => {
                                                    if(counter % 4 == 0){
                                                        counter++;
                                                        return <span className="col s4"><li key={ingridient.id} ><IngridientItem userIngridients={this.props.userIngridients} addIngridientToUser={ this.props.addIngridientToUser } removeIngridientFromUser= {this.props.removeIngridientFromUser} ingridient={ ingridient }/></li></span>;
                                                    } else {
                                                        counter++;
                                                        return <span className="col s4"><li key={ingridient.id} ><IngridientItem userIngridients={this.props.userIngridients} addIngridientToUser={ this.props.addIngridientToUser } removeIngridientFromUser= {this.props.removeIngridientFromUser} ingridient={ ingridient }/></li></span>  ;
                                                    }
                                                })
                                        :
                                        null
                                        }
                                    </ul>
                                </div>  
                            </Collapsible>
   
					    </div>
                        
                        <div className="category-item text-center">
                            <Collapsible trigger={<span><img src={require('../Sauces.png')} alt="milk" width="48" height="48" /><br /> Sauces</span>}>
                                <div className="row">
                                    <ul className='ingridients-list'>
                                        { this.state.ingridientsSauce && this.state.ingridientsSauce.length !== 0
                                        ?
                                        this.state.ingridientsSauce.map(ingridient => {
                                                    if(counter % 4 == 0){
                                                        counter++;
                                                        return <span className="col s4"><li key={ingridient.id} ><IngridientItem userIngridients={this.props.userIngridients} addIngridientToUser={ this.props.addIngridientToUser } removeIngridientFromUser= {this.props.removeIngridientFromUser} ingridient={ ingridient }/></li></span>;
                                                    } else {
                                                        counter++;
                                                        return <span className="col s4"><li key={ingridient.id} ><IngridientItem userIngridients={this.props.userIngridients} addIngridientToUser={ this.props.addIngridientToUser } removeIngridientFromUser= {this.props.removeIngridientFromUser} ingridient={ ingridient }/></li></span>  ;
                                                    }
                                                })
                                        :
                                        null
                                        }
                                    </ul>
                                </div>  
                            </Collapsible>
   
					    </div>
                    </div>
               
                
            );
        }
    }
    
    export default SearchIngridientByCatagory;