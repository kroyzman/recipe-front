import React, { Component } from "react";
import SearchReciepRadioButtons from './SearchReciepRadioButtons';
import ReciepViewer from './ReciepViewer';
import RecommendedRecipes from './RecommendedRecipes';
import { Link, NavLink} from 'react-router-dom'

class SearchReciep extends Component {
    constructor(props){
        super(props);

        if(!this.props.loggedIn){
            this.props.history.push('/login')
        }
        this.state = {
            searchQuery: "",
            withIngridients: false,
            recieps: [],
            recipesAfterPagination: [],
            currentPageRecipes: 0,
            recipesMissingOne: [],
            recipesMissingOneAfterPagination: [],
            currentPageRecipesOne: 0,
            recipesMissingTwo: [],
            recipesMissingTwoAfterPagination: [],
            currentPageRecipesTwo: 0,
            recipesMissingThree: [],
            recipesMissingThreeAfterPagination: [],
            currentPageRecipesThree: 0,
            currentPage: 0,
            reciepsPerPage: 9,
            totalCount: 0,
            catagoryList: [],
            isWithIngridientsRecipesDisplayed: false,
            recommendedRecipes: [],
            recommendedRecipesWithIngridients: [],
            recommendedByRating: []
        }

        this.searchInputChange = this.searchInputChange.bind(this);
        this.fetchSearchreciepsResult = this.fetchSearchreciepsResult.bind(this);
        this.searchRecipeClick = this.searchRecipeClick.bind(this);
        this.handleWithIngridients = this.handleWithIngridients.bind(this);
        this.handleOnClickPagination = this.handleOnClickPagination.bind(this);
        this.fetchTotalCountOfMatchResults = this.fetchTotalCountOfMatchResults.bind(this);
        this.handleCatagory = this.handleCatagory.bind(this);
        this.fetchSearchRecipesByIngridientsResult = this.fetchSearchRecipesByIngridientsResult.bind(this);
        this.handleOnClickPaginationWithIngridients = this.handleOnClickPaginationWithIngridients.bind(this);
        this.initRecipes = this.initRecipes.bind(this);    
        this.fetchRecommendRecipes = this.fetchRecommendRecipes.bind(this);
    }

    componentDidMount(){
        let counter = 0;
        this.searchRecipeClick();

        this.fetchRecommendRecipes((recommendedRecipes) => {
            this.setState({
              recommendedRecipes: recommendedRecipes
            }, () => console.log("fetch recommend recipes" + recommendedRecipes))
          })
        
          this.fetchGetUser((user) => {
            this.fetchAllRecipesFromDB((recipes) => {
                user.userRatesRecipes.sort(function(a, b) {
                    return b.userRate - a.userRate;
                });
               
                for(let i = 0 ; i < user.userRatesRecipes.length && counter !== 8; i++){
                    for(let j = 0 ; j < recipes.length && counter !== 8 ; j++){
                        if(user.userRatesRecipes[i].ratedRecipeId == recipes[j].id){
                            counter++;
                            this.state.recommendedByRating.push(recipes[j])
                        }
                    }
                }
                console.log(user)
                console.log(this.state.recommendedByRating)
                this.forceUpdate();
            });
          this.forceUpdate();
        })

        this.forceUpdate()
       
    }

    fetchGetUser(callback){
        let url = 'http://localhost:8080/recipeforu/users/' + "/" + this.props.user.email;

        fetch(url, {
        method: 'GET',
        headers:{
            'Content-Type': 'application/json'
        }
        }).then(response => {return response.json();})
            .then(response => {return callback(response)})
            .catch(error => {return console.log(error)});
    }

    fetchAllRecipesFromDB(callback){
        let url = 'http://localhost:8080/recipeforu/recipe_all';
    
        fetch(url, {
          method: 'GET',
          headers:{
            'Content-Type': 'application/json'
          }
          }).then(response => {return response.json();})
          .then(response => {return callback(response)})
          .catch(error => {return console.log(error)});
      }
      
      fetchRecommendRecipes(callback){
        let url = 'http://localhost:8080/recipeforu/users/recommend_event/';
    
        fetch(url + this.props.recommendUserId, {
          method: 'GET',
          headers:{
            'Content-Type': 'application/json'
          }
          }).then(response => {return response.json();})
          .then(response => {return callback(response)})
          .catch(error => {return console.log(error)});
      }

    searchInputChange(e){
        let searchQuery = e.target.value;
  
        this.setState({
          searchQuery: searchQuery
        })
    }

    fetchTotalCountOfMatchResults(callback){
        let url = 'http://localhost:8080/recipeforu/recieps/count/' + this.state.searchQuery ;

        if(this.state.catagoryList.length !== 0){ 
            url += "&";

            for(let i = 0 ; i < this.state.catagoryList.length ; i++){
                url += "myParam=" + this.state.catagoryList[i];
                if(this.state.catagoryList.length - 1 !== i){
                    url += "&"
                }
            }
        }

        fetch(url, {
        method: 'GET',
        headers:{
            'Content-Type': 'application/json'
        }
        }).then(response => {return response.json();})
        .then(response => {return callback(response)})
        .catch(error => console.error('Error:', error));
    }

    searchRecipeClick(){
        this.setState({
            recieps: [],
            recipesMissingOne: [],
            recipesMissingTwo: [],
            recipesMissingThree: [],
            
        })

        this.fetchTotalCountOfMatchResults((totalCount) => {
            this.setState({
               totalCount: totalCount
            })

              
        })

        if(this.state.withIngridients){
            console.log("with ingridients!!!")
            this.fetchSearchRecipesByIngridientsResult((recieps) => {
                console.log(recieps["Matched"])
                console.log(recieps["MissingOne"])
                console.log(recieps["MissingTwo"])
                console.log(recieps["MissingThree"])
                this.setState({
                    recieps: recieps["Matched"],
                    recipesMissingOne: recieps["MissingOne"],
                    recipesMissingTwo: recieps["MissingTwo"],
                    recipesMissingThree: recieps["MissingThree"]
                }, () => this.initRecipes())
                  
                          })
        } else {
            this.fetchSearchreciepsResult(this.state.searchQuery ,(recieps) => {
                this.setState({
                    recieps: recieps
                  })
                
              })
        }   
    }

    initRecipes(){
        if(this.state.recieps.length < this.state.reciepsPerPage){
            this.state.recipesAfterPagination = [...this.state.recieps]
        } else {
            for(let i = 0 ; i < this.state.reciepsPerPage ; i++){
                this.state.recipesAfterPagination.push(this.state.recieps[i])
            }
        }

        if(this.state.recipesMissingOne.length < this.state.reciepsPerPage){
            this.state.recipesMissingOneAfterPagination = [...this.state.recipesMissingOne]
        } else {
            for(let i = 0 ; i < this.state.reciepsPerPage ; i++){
                this.state.recipesMissingOneAfterPagination.push(this.state.recipesMissingOne[i])
            }
        }

        if(this.state.recipesMissingTwo.length < this.state.reciepsPerPage){
            this.state.recipesMissingTwoAfterPagination = [...this.state.recipesMissingTwo]
        } else {
            for(let i = 0 ; i < this.state.reciepsPerPage ; i++){
                this.state.recipesMissingTwoAfterPagination.push(this.state.recipesMissingTwo[i])
            }
        }

        if(this.state.recipesMissingThree.length < this.state.reciepsPerPage){
            this.state.recipesMissingThreeAfterPagination = [...this.state.recipesMissingThree]
        } else {
            for(let i = 0 ; i < this.state.reciepsPerPage ; i++){
                this.state.recipesMissingThreeAfterPagination.push(this.state.recipesMissingThree[i])
            }
        }

        console.log(this.state.recommendedRecipes)
        console.log(this.state.recieps)
        for(let i = 0 ; i < this.state.recommendedRecipes.length ; i++){
            for(let j = 0 ; j < this.state.recieps.length ; j++){
                if(this.state.recommendedRecipes[i].id === this.state.recieps[j].id){
                    console.log("recommended recipe with ingridients is found!!!!!!!!!!!!!!!!!!!!!!!")
                    this.state.recommendedRecipesWithIngridients.push(this.state.recommendedRecipes[i])
                }
            }
            
        }
        console.log(this.state.recommendedRecipesWithIngridients)
       
    }

    
    fetchSearchRecipesByIngridientsResult(callback){
        let url = 'http://localhost:8080/recipeforu/recipe_ingridients';
        fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                ingridients: this.props.userIngridients
                
            })
        }).then(response => {return response.json();})
        .then(response => {return callback(response)})
        .catch(error => console.error('Error:', error));
    }


    fetchSearchreciepsResult(searchQuery, callback){
        let url = 'http://localhost:8080/recipeforu/recieps/' + searchQuery + "?page=" + this.state.currentPage;
        
        
        if(this.state.catagoryList.length !== 0){ 
            url += "&";

            for(let i = 0 ; i < this.state.catagoryList.length ; i++){
                url += "myParam=" + this.state.catagoryList[i];
                if(this.state.catagoryList.length - 1 !== i){
                    url += "&"
                }
            }
        }
        fetch(url, {
        method: 'GET',
        headers:{
            'Content-Type': 'application/json'
        }
        }).then(response => {return response.json();})
        .then(response => {return callback(response)})
        .catch(error => console.error('Error:', error));
    }

    handleWithIngridients(withIngridients){
        
        this.setState({
            withIngridients: withIngridients
        }, () => {
            this.searchRecipeClick()
        });

        
    }

    handleOnClickPagination(e){
        this.setState(
            {currentPage : e.target.id}, () => this.searchRecipeClick()
        )   
    }

    handleOnClickPaginationWithIngridients(e, listName){
        if(listName == "Match"){
            this.setState({
                currentPageRecipes: e.target.id,
                recipesAfterPagination: []
            }, () => {
                let i = this.state.currentPageRecipes * this.state.reciepsPerPage
                let endI = this.state.reciepsPerPage + i
               
                if(i + this.state.reciepsPerPage > this.state.recieps.length) {
                    endI = this.state.recieps.length
                }   
                
                console.log("start i -> " + i)
                console.log("end i -> " + endI)

                for( ; i < endI; i++)
                    {
                        this.state.recipesAfterPagination.push(this.state.recieps[i])
                    }
                    this.forceUpdate()
                }
                
            )
        }
        else if(listName == "One"){
            this.setState({
                currentPageRecipesOne: e.target.id,
                recipesMissingOneAfterPagination: []
            }, () => {
                let i = this.state.currentPageRecipesOne * this.state.reciepsPerPage
                let endI = this.state.reciepsPerPage + i
               
                if(i + this.state.reciepsPerPage > this.state.recipesMissingOne.length) {
                    endI = this.state.recipesMissingOne.length
                }               

                for( ; i < endI; i++)
                    {
                        this.state.recipesMissingOneAfterPagination.push(this.state.recipesMissingOne[i])
                    }
                    this.forceUpdate()
                }
                
            )
        } else if(listName == "Two") {
            this.setState({
                currentPageRecipesTwo: e.target.id,
                recipesMissingTwoAfterPagination: []
            }, () => {
                let i = this.state.currentPageRecipesTwo * this.state.reciepsPerPage
                let endI = this.state.reciepsPerPage + i
               
                if(i + this.state.reciepsPerPage > this.state.recipesMissingTwo.length) {
                    endI = this.state.recipesMissingTwo.length
                }               

                for( ; i < endI; i++)
                    {
                        this.state.recipesMissingTwoAfterPagination.push(this.state.recipesMissingTwo[i])
                    }
                    this.forceUpdate()
                }
                
            )
        } else {
            this.setState({
                currentPageRecipesThree: e.target.id,
                recipesMissingThreeAfterPagination: []
            }, () => {
                let i = this.state.currentPageRecipesThree * this.state.reciepsPerPage
                let endI = this.state.reciepsPerPage + i
               
                if(i + this.state.reciepsPerPage > this.state.recipesMissingThree.length) {
                    endI = this.state.recipesMissingThree.length
                }               

                console.log("start i -> " + i)
                console.log("end i -> " + endI)

                for( ; i < endI; i++)
                    {
                        this.state.recipesMissingThreeAfterPagination.push(this.state.recipesMissingThree[i])
                    }
                    console.log(this.state.recipesMissingThreeAfterPagination)
                    this.forceUpdate()
                }
                
            )

        }
    }

    handleCatagory(catagory){
        for(let i = 0 ; i < this.state.catagoryList.length ; i++){
            if(this.state.catagoryList[i] == catagory){
                this.state.catagoryList.splice(i, 1)
                console.log(this.state.catagoryList)
                return;
            }
        }
        this.state.catagoryList.push(catagory)
        
        console.log(this.state.catagoryList)
    }

    render() {
        const paginationNumbers = [];
        const paginationNumbersOne = [];
        const paginationNumbersTwo = [];
        const paginationNumbersThree = [];

        if(this.state.withIngridients){
            for(let i = 0; i < this.state.recieps.length / this.state.reciepsPerPage; i++){
                if(this.state.currentPageRecipes == i){
                    paginationNumbers.push(<li  className="active" ><Link id={i} onClick={(e) => this.handleOnClickPaginationWithIngridients(e, "Match")}>{i}</Link></li>)
                } else {
                    paginationNumbers.push(<li  className="waves-effect" ><Link id={i} onClick={(e) => this.handleOnClickPaginationWithIngridients(e, "Match")}>{i}</Link></li>)
                }  
            }
        } else {
            for(let i = 0; i < this.state.totalCount / this.state.reciepsPerPage; i++){
                if(this.state.currentPage == i){
                    paginationNumbers.push(<li  className="active" ><Link id={i} onClick={this.handleOnClickPagination}>{i}</Link></li>)
                } else {
                    paginationNumbers.push(<li  className="waves-effect" ><Link id={i} onClick={this.handleOnClickPagination}>{i}</Link></li>)
                }   
            }
        }
        
        for(let i = 0; i < this.state.recipesMissingOne.length / this.state.reciepsPerPage; i++){
            if(this.state.currentPageRecipesOne == i){
                paginationNumbersOne.push(<li  className="active" ><Link id={i} onClick={(e) => this.handleOnClickPaginationWithIngridients(e, "One")}>{i}</Link></li>)
            } else {
                paginationNumbersOne.push(<li  className="waves-effect" ><Link id={i} onClick={(e) => this.handleOnClickPaginationWithIngridients(e, "One")}>{i}</Link></li>)
            }  
        }

        for(let i = 0; i < this.state.recipesMissingTwo.length / this.state.reciepsPerPage; i++){
            if(this.state.currentPageRecipesTwo == i){
                paginationNumbersTwo.push(<li  className="active" ><Link id={i} onClick={(e) => this.handleOnClickPaginationWithIngridients(e, "Two")}>{i}</Link></li>)
            } else {
                paginationNumbersTwo.push(<li  className="waves-effect" ><Link id={i} onClick={(e) => this.handleOnClickPaginationWithIngridients(e, "Two")}>{i}</Link></li>)
            }  
        }

        for(let i = 0; i < this.state.recipesMissingThree.length / this.state.reciepsPerPage; i++){
            if(this.state.currentPageRecipesThree == i){
                paginationNumbersThree.push(<li  className="active" ><Link id={i} onClick={(e) => this.handleOnClickPaginationWithIngridients(e, "Three")}>{i}</Link></li>)
            } else {
                paginationNumbersThree.push(<li  className="waves-effect" ><Link id={i} onClick={(e) => this.handleOnClickPaginationWithIngridients(e, "Three")}>{i}</Link></li>)
            }  
        }

        return (
            <div className="container center">
                <h3 className="title">Search For Recipe</h3>
                {!this.state.withIngridients
                ?
                <div>
                    <SearchReciepRadioButtons withIngridients={this.handleWithIngridients} catagory={this.handleCatagory}/> 
                    <div>
                        <input type='text' id="search_recipe" placeholder="Recipe" onChange={ (e) => this.searchInputChange(e) }/>
                     </div>
                    <div className='btn btnClr' onClick={ this.searchRecipeClick }>
                     Search
                    </div>
                    <RecommendedRecipes isReccommendedByRating={false} recommendUserId={this.props.recommendUserId} recommendedRecipes={this.state.recommendedRecipes} userName={this.props.userName}/>
                    <RecommendedRecipes isReccommendedByRating={true} recommendUserId={this.props.recommendUserId} recommendedRecipes={this.state.recommendedByRating} userName={this.props.userName}/>
                    
                </div>
                :
                <div>
                    <SearchReciepRadioButtons withIngridients={this.handleWithIngridients} catagory={this.handleCatagory}/>
                    <RecommendedRecipes recommendUserId={this.props.recommendUserId} recommendedRecipes={this.state.recommendedRecipesWithIngridients} userName={this.props.userName}/>
                    {!this.state.isWithIngridientsRecipesDisplayed
                    ?
                    <div>
                        {this.searchRecipeClick()}
                        {this.setState({isWithIngridientsRecipesDisplayed: true})}
                    </div>
                    :
                    null
                    }
                    
                </div>
                }
                
                
                    {this.state.recieps.length == 0
                    ?
                        <h3 className="title">No Matching Recipes</h3>
                    :
                        null
                    }
                    {this.state.recieps.length !== 0
                    ?
                    <div>
                        <ReciepViewer recieps={this.state.withIngridients? this.state.recipesAfterPagination : this.state.recieps} handleFavouriteRecipe={this.props.handleFavouriteRecipe} checkForFavouriteRecipe={this.props.checkForFavouriteRecipe} removeFavouriteRecipe={this.props.removeFavouriteRecipe}/>    
                        <ul className="pagination">

                            {paginationNumbers}
                            
                        </ul>
                    {this.state.recipesMissingOne && this.state.recipesMissingOne.length !== 0
                    ?
                    <div>
                        <h3 className="title">Recipes Missing One Ingredients</h3>
                        <ReciepViewer recieps={this.state.recipesMissingOneAfterPagination} handleFavouriteRecipe={this.props.handleFavouriteRecipe} checkForFavouriteRecipe={this.props.checkForFavouriteRecipe} removeFavouriteRecipe={this.props.removeFavouriteRecipe}/>
                        <ul className="pagination">

                            {paginationNumbersOne}
                            
                        </ul>                   
                    </div>
                    :
                    null
                    }
                    {this.state.recipesMissingTwo && this.state.recipesMissingTwo.length !== 0
                    ?
                    <div>
                        <h3 className="title">Recipes Missing Two Ingredients</h3>
                        <ReciepViewer recieps={this.state.recipesMissingTwoAfterPagination} handleFavouriteRecipe={this.props.handleFavouriteRecipe} checkForFavouriteRecipe={this.props.checkForFavouriteRecipe} removeFavouriteRecipe={this.props.removeFavouriteRecipe}/>
                        <ul className="pagination">

                            {paginationNumbersTwo}
                            
                        </ul>
                    </div>
                    :
                    null
                    }
                    {this.state.recipesMissingThree && this.state.recipesMissingThree.length !== 0
                    ?
                    <div>
                        <h3 className="title">Recipes Missing Three Ingredients</h3>
                        <ReciepViewer recieps={this.state.recipesMissingThreeAfterPagination} handleFavouriteRecipe={this.props.handleFavouriteRecipe} checkForFavouriteRecipe={this.props.checkForFavouriteRecipe} removeFavouriteRecipe={this.props.removeFavouriteRecipe}/>
                        <ul className="pagination">

                            {paginationNumbersThree}
                            
                        </ul>
                    </div>
                    :
                    null
                    }
                    
                    </div>
                    :
                    <div>
                    {this.state.recipesMissingOne && this.state.recipesMissingOne.length !== 0
                        ?
                        <div>
                            <h3 className="title">Recipes Missing One Ingredient</h3>
                            <ReciepViewer recieps={this.state.recipesMissingOne} handleFavouriteRecipe={this.props.handleFavouriteRecipe} checkForFavouriteRecipe={this.props.checkForFavouriteRecipe} removeFavouriteRecipe={this.props.removeFavouriteRecipe}/>
                        </div>
                        :
                        null
                        }
                        {this.state.recipesMissingTwo && this.state.recipesMissingTwo.length !== 0
                        ?
                        <div>
                            <h3 className="title">Recipes Missing Two Ingredients</h3>
                            <ReciepViewer recieps={this.state.recipesMissingTwo} handleFavouriteRecipe={this.props.handleFavouriteRecipe} checkForFavouriteRecipe={this.props.checkForFavouriteRecipe} removeFavouriteRecipe={this.props.removeFavouriteRecipe}/>
                        </div>
                        :
                        null
                        }
                        {this.state.recipesMissingThree && this.state.recipesMissingThree.length !== 0
                        ?
                        <div>
                            <h3 className="title">Recipes Missing Three Ingredients</h3>
                            <ReciepViewer recieps={this.state.recipesMissingThree} handleFavouriteRecipe={this.props.handleFavouriteRecipe} checkForFavouriteRecipe={this.props.checkForFavouriteRecipe} removeFavouriteRecipe={this.props.removeFavouriteRecipe}/>
                        </div>
                        :
                        null
                        }
                        </div>
                    }
               
               
            </div>
        );
    }
}

export default SearchReciep;