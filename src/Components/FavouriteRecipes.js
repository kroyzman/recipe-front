import React, { Component } from 'react';
import { Link, NavLink} from 'react-router-dom';

const style = {
    margin: '70px',
  };

class FavouriteRecipes extends React.Component {
    constructor(props){
        super(props)
        if(!this.props.loggedIn){
            this.props.history.push('/login')
        }
        this.removeFavouriteRecipe = this.removeFavouriteRecipe.bind(this);
    }

    
    removeFavouriteRecipe(recipe){
        this.forceUpdate();
        this.props.removeFavouriteRecipe(recipe)
    }

    render() {
        let {recieps} = this.props;

        return (
            <div className="container center">
                <h3 className="title" style={style} >My Favourite Recipes</h3>
            <div className="row">
                <ul className='recieps-list'>
                    { recieps
                            ?
                            recieps.map(reciep => {
                                return   (
                                            <div className="col s12 m4">
                                                <div className="card">
                                                    <div className="card-image">
                                                        <img src={reciep.img}/>
                                                        <span className="card-title">{reciep.title}</span>
                                                        <a className="btn-floating halfway-fab waves-effect waves-light red" onClick={() => this.removeFavouriteRecipe(reciep)}><i className="material-icons">remove_circle_outline</i></a>
                                                    </div>
                                                    <div className="card-content">
                                                        <p className="pa">
                                                            <div>Course: {reciep.instruction.course}</div>
                                                            <div>Cuisine: {reciep.instruction.cuisine}</div>
                                                            <div>Prep Time: {reciep.instruction.prepTimeInSeconds / 60} Minutes</div>
                                                        </p>
                                                    <p><Link to={"/recipe/" + reciep.id} className="btn btnClr recipe">Go To Recipe</Link></p>
                                                    </div>
                                                </div>
                                                <span className="font color_font">{reciep.title}</span>
                                                
                                            </div>
                                        )
                            })
                            :
                            null
                        }
                </ul>
            </div>
        </div>
            
        )
    }
}

export default FavouriteRecipes