import React, { Component } from "react";
import './RecipeRating.css'


class RecipeRating extends Component {
    constructor(props) {
        super(props);
        this.state ={  
            checkedAttr: [false, false, false, false, false],
            currentRating: 0,
            totalRatingsCount: this.props.recipe.totalCountOfRatings,
            user: null,
            isRated: false
        }

        this.fetchGetUser((user) => {
            console.log("IN HERHERHEHRHERHEHR " + user)
            this.setState({
                user: user
            }, () => this.setCurrentRating(user))
          
        })
      
        this.setCurrentRating = this.setCurrentRating.bind(this)
        this.handleRatingChange = this.handleRatingChange.bind(this)
        this.fetchUserRecipeRating = this.fetchUserRecipeRating.bind(this)
        this.updateRecipeRatings = this.updateRecipeRatings.bind(this)
        this.fetchRecipeOverallRating = this.fetchRecipeOverallRating.bind(this)
      }

      componentDidMount(){
          this.fetchGetUser((user) => {
              console.log("IN HERHERHEHRHERHEHR " + user)
              this.setState({
                  user: user
              }, () => this.setCurrentRating(user))
            
          })
        
      }


      setCurrentRating(user){
        console.log(user)
        console.log(this.props.recipe.id)
          for(let i = 0 ; i < user.userRatesRecipes.length ; i++){
            console.log(user.userRatesRecipes[i].ratedRecipeId )
              if(user.userRatesRecipes[i].ratedRecipeId == this.props.recipe.id){
                  this.setState({
                      currentRating: user.userRatesRecipes[i].userRate,
                      isRated: true
                  })
                
                    for(let i = 0 ; i < this.state.checkedAttr.length ; i++)
                    {
                        this.state.checkedAttr[i] = false;
                    }
                    console.log(user.userRatesRecipes[i].userRate)
                    this.state.checkedAttr[user.userRatesRecipes[i].userRate - 1] = true;
                }
          }

          this.forceUpdate();
      }

      fetchGetUser(callback){
        let url = 'http://localhost:8080/recipeforu/users/' + "/" + this.props.user.email;

        fetch(url, {
        method: 'GET',
        headers:{
            'Content-Type': 'application/json'
        }
        }).then(response => {return response.json();})
            .then(response => {return callback(response)})
            .catch(error => {return console.log(error)});
    }

      updateRecipeRatings(currentRating){
        console.log("in herer")
        this.fetchRecipeOverallRating(currentRating, (recipe) => {
            this.setState({
                totalRatingsCount: recipe.totalCountOfRatings,
            })
            console.log(recipe)
            this.props.updateAvgRating(recipe.avgRating)
        })
      }

      fetchRecipeOverallRating(currentRating, callback){
        let url = 'http://localhost:8080/recipeforu/recipe';
        
        fetch(url + "/" + this.props.recipe.id + "/" + currentRating, {
            method: 'POST',
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(
                this.props.user
            )
            }).then(response => {return response.json();})
            .then(response => {return callback(response)})
            .catch(error => console.error('Error:', error));
    }

      handleRatingChange(e){

            const currentRating = Number(e.target.value) + 1
            
            for(let i = 0 ; i < this.state.checkedAttr.length ; i++)
            {
                this.state.checkedAttr[i] = false;
            }

            this.state.checkedAttr[e.target.value] = true;
            
            this.setState({
                currentRating: currentRating }
                
            )
            
            this.fetchUserRecipeRating(currentRating, (user) => {
                this.updateRecipeRatings(currentRating)
                this.setState({
                    isRated: true
                })
            })
        
        }
    

        fetchUserRecipeRating(currentRating, callback){
            let url = 'http://localhost:8080/recipeforu/users/rating';
      
            fetch(url + '/' + this.props.user.email, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    // userEmail: this.props.user.email,
                    userRate: currentRating,
                    ratedRecipeId: this.props.recipe.id
                    
                })
            }).then(response => {return callback(response).json();})
            .then(response => {return callback(response)})
            .catch(error => console.error('Error:', error));
        }

      render() {

        return (
            <div class="container">
                <div class="hreview-aggregate">
                    <div class="row">
                        <div class="col s12 m6 l6">
                        <meta itemprop="worstRating" content="1"/>
                        <meta itemprop="bestRating" content="5"/>
                        <meta itemprop="reviewCount" content="1"/>
                        </div>
                        <div class="row">
                            <div class="score col s12">
                            {this.state.currentRating}
                            </div>
                            <div class="rating-stars col s12">
                            {!this.state.isRated
                            ?
                            <span>
                            <input type="radio" name="stars" id="star-null"/>
                            <input type="radio" name="stars" id="star-1" value="0" saving="1" data-start="1" checked={this.state.checkedAttr[0]} onChange={this.handleRatingChange}/>
                            <input type="radio" name="stars" id="star-2" value="1" saving="2" data-start="2" checked={this.state.checkedAttr[1]} onChange={this.handleRatingChange}/>
                            <input type="radio" name="stars" id="star-3" value="2" saving="3" data-start="3" checked={this.state.checkedAttr[2]} onChange={this.handleRatingChange}/>
                            <input type="radio" name="stars" id="star-4" value="3" saving="4" data-start="4" checked={this.state.checkedAttr[3]} onChange={this.handleRatingChange}/>
                            <input type="radio" name="stars" id="star-5" value="4" saving="5" checked={this.state.checkedAttr[4]} onChange={this.handleRatingChange}/>
                            </span>
                            :
                            <span>Thank You!
                            
                            </span>
                            }
                            {!this.state.isRated
                            ?
                            <section>
                            <label for="star-1">
                                <svg width="255" height="240" viewBox="0 0 51 48">
                                    <path d="m25,1 6,17h18l-14,11 5,17-15-10-15,10 5-17-14-11h18z"></path>
                                </svg>
                            </label>
                            <label for="star-2">
                                <svg width="255" height="240" viewBox="0 0 51 48">
                                    <path d="m25,1 6,17h18l-14,11 5,17-15-10-15,10 5-17-14-11h18z"></path>
                                </svg>
                            </label>
                            <label for="star-3">
                                <svg width="255" height="240" viewBox="0 0 51 48">
                                    <path d="m25,1 6,17h18l-14,11 5,17-15-10-15,10 5-17-14-11h18z"></path>
                                </svg>
                            </label>
                            <label for="star-4">
                                <svg width="255" height="240" viewBox="0 0 51 48">
                                    <path d="m25,1 6,17h18l-14,11 5,17-15-10-15,10 5-17-14-11h18z"></path>
                                </svg>
                            </label>
                            <label for="star-5">
                                <svg width="255" height="240" viewBox="0 0 51 48">
                                    <path d="m25,1 6,17h18l-14,11 5,17-15-10-15,10 5-17-14-11h18z"></path>
                                </svg>
                            </label>
                        </section>
                        :
                        null
                            }
                            
                            
                            </div>
                            <div class="reviews-stats col s12">
                            <span class="reviewers-small"></span>
                            <span class="reviews-num">
                                {this.state.totalRatingsCount}
                                </span> total
                            </div>
                        
                        </div>
                     
                    </div>
                </div>
          </div>
       )
      }
    }

export default RecipeRating;