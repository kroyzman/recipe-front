import React, { Component } from "react";
import './ScanReciept.css'


class ScanReciept extends Component {
    constructor(props) {
        super(props);
        if(!this.props.loggedIn){
          this.props.history.push('/login')
      }
        this.state ={
          file:null,
          isLoading: false
        }
        this.onFormSubmit = this.onFormSubmit.bind(this)
        this.onChange = this.onChange.bind(this)
        this.fetchFileUpload = this.fetchFileUpload.bind(this)
       
      }

      fetchFileUpload(callback){
        let url = 'http://localhost:8080/recipeforu/reciept_upload';

        let data = new FormData();
        data.append('file', this.state.file);
        data.append('user', this.props.user.email);

        fetch(url, {
            method: 'POST',
            body: data
        }).then(response => {return response.json();})
        .then(response => {return callback(response)})
        .catch(error => console.error('Error:', error));
    }

      onFormSubmit(e){
        this.setState({
          isLoading: true
        })
        
        e.preventDefault()
        
        if(this.state.file != null){
          console.log(this.state.file)
          this.fetchFileUpload((user) => {
            this.setState({
              isLoading: false
            })
            this.props.updateUser(user)
          })
        }
        
      }

      onChange(e) {
        this.setState({file:e.target.files[0]})
      }
    //   fileUpload(file){
    //     const url = 'http://example.com/file-upload';
    //     const formData = new FormData();
    //     formData.append('file',file)
    //     const config = {
    //         headers: {
    //             'content-type': 'multipart/form-data'
    //         }
    //     }
    //     return  post(url, formData,config)
    //   }
    
      render() {
        return (
        <div className="container center">
        <h3 id="upload-h3" className="title">Upload Your Reciept</h3>
        <img id="reciept_img" className="responsive-img" alt="Upload" src={require('../supermarket-receipt.jpg')}></img>
        <img id="reciept_img" className="responsive-img" alt="Upload" src={require('../arrow-upload-icon.png')}></img>
        {this.state.isLoading
        ?
        <div class="preloader-wrapper active">
        <div class="spinner-layer spinner-red-only">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div><div class="gap-patch">
            <div class="circle"></div>
          </div><div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>
      </div>
      :
      null}
        <form onSubmit={this.onFormSubmit} enctype="multipart/form-data">
        <div class="file-field input-field">
          <div class="btn btnClr">
            <span>File</span>
            <input type="file" name="file" onChange={this.onChange}/>
          </div>
          <div class="file-path-wrapper">
            <span>{this.state.file
            ?
            this.state.file.name
            :
            null
            }</span>
            <input type="text"/>
            
            
          </div>

          
        </div>
        <button className="btn btnClr" type="submit">Upload</button>
      </form>

      </div>
       )
      }
    }

export default ScanReciept;