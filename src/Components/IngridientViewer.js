import React, { Component } from 'react';
import IngridientItem from './IngridientItem';

class IngridientViewer extends React.Component {
    constructor(props){
        super(props)
    }


    render() {
        let {ingridients} = this.props;
        

        return (
            <div className="container">
                <div className="row">
                    <ul className='ingridients-list'>
                        { ingridients
                                ?
                                ingridients.map(ingridient => {
                                    
                                        return (<div className="col s12 m4">
                                                    <li key={ingridient.id} ><IngridientItem userIngridients={this.props.userIngridients} addIngridientToUser={ this.props.addIngridientToUser } removeIngridientFromUser= {this.props.removeIngridientFromUser} ingridient={ ingridient }/></li>   
                                                </div>)
                                })
                                :
                                null
                            }
                    </ul>
                </div>
            </div>
            
        )
    }
}

export default IngridientViewer