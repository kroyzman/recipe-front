import React, { Component } from 'react';
import RecommendedRecipesViewer from './RecommendedRecipeViewer';

class RecommendedRecipes extends React.Component {

    constructor(props){
        super(props)
        this.state = {
        }

        console.log("SDFSDFSDFSDFSDFSDFSDFSDFSDFSDF " + this.props.recommendedRecipes)

    }


    render() {
        return (
            <div id="recommend-div">
                {this.props.recommendedRecipes.length !== 0
                ?
                <span>
                    {this.props.isReccommendedByRating
                    ?
                        <h3 className="title">Recommended Recipes For {this.props.userName}</h3>
                    :
                        <h3 className="title">You Will Also Like...</h3>
                    }
                    <RecommendedRecipesViewer recommendedRecipes={this.props.recommendedRecipes}/>
                    <div className="divider"></div>
                </span>
                :
                <span>
                    <RecommendedRecipesViewer recommendedRecipes={this.props.recommendedRecipes}/>
                    <div className="divider"></div>
                </span>
                }
               
            </div>
        )
    }

}

export default RecommendedRecipes