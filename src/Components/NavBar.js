import React from 'react'
import { Link, NavLink} from 'react-router-dom'

const Navbar = (props) => {
    return (
        <nav className="nav-wrapper pink darken-3">
            <Link to="/" className="brand-logo">RecipeForU</Link>
            <div className="container">
            {props.user
                ?
                <div>
                    <ul className="right">
                        <li><Link to="/MyIngridients">My ingredients<span className="badge white-text">{props.user.ingridients.length}</span></Link></li>
                        <li><Link to="/FavouriteRecipes">Favourite Recipes</Link></li>
                        <li><Link to="/" onClick={props.onLogout}>Logout</Link></li>
                    </ul>
                    <div>
                        <ul className="left">
                            <li><Link to="/searchIngridient" >Add ingredients</Link></li>
                            <li><Link to="/searchRecipe" >Search Recipes</Link></li>
                            <li><Link to="/scanReciept" >Scan Reciept</Link></li>
                        </ul>
                    </div>
                </div>
                :
                <ul className="right">
                    <li><Link to="/signup">Signup</Link></li>
                    <li><Link to="/login">Login</Link></li>
                </ul>
                }
                
            </div>
        </nav>
    )
}

export default Navbar