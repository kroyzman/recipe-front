import React, { Component } from "react";
import SearchIngridientByTyping from "./SearchIngridientByTyping";
import SearchIngridientByCatagory from "./SearchIngridientByCatagory";

class SearchIngridientRadioButtons extends Component {
    constructor(props){
        super(props);
        this.state = {
            option: "option1"
        }
        this.handleOptionChange = this.handleOptionChange.bind(this);
    }

    handleOptionChange(changeEvent){
        console.log(changeEvent.currentTarget.value);
        this.setState({
            option: changeEvent.currentTarget.value
          });
    }

    
    render() {
        return (
            <div className="center">
                <h3 className="center title">Search For Ingredients</h3>
                <form>
                    <p>
                    <label>
                        <input className="with-gap" name="group1" value="option1" type="radio" checked={this.state.option === "option1"} onChange={this.handleOptionChange} />
                        <span>Type Them</span>
                    </label>
                    </p>
                    <p>
                    <label>
                        <input className="with-gap" name="group1" value="option2" checked={this.state.option === "option2"} onChange={this.handleOptionChange} type="radio" />
                        <span>Choose From Catagory</span>
                    </label>
                    </p>
                </form>
                {this.state.option === "option1"
                    ?
                    <SearchIngridientByTyping userIngridients={this.props.userIngridients} addIngridientToUser={this.props.addIngridientToUser} removeIngridientFromUser={this.props.removeIngridientFromUser}/>
                    :
                    <SearchIngridientByCatagory userIngridients={this.props.userIngridients} addIngridientToUser={this.props.addIngridientToUser} removeIngridientFromUser={this.props.removeIngridientFromUser}/>                }
            </div>
            
        );
    }
}

export default SearchIngridientRadioButtons;