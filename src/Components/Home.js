import React, { Component } from 'react';
import './Home.css';
import BottomContent from './BottomContent';

class Home extends React.Component {
    constructor(props){
        super(props)
    }

    render() {
        return (
            <div>
            <div className="container">
            {this.props.user
                ?
                <div id="welcome">    
                    <h2 className="center" >Welcome {this.props.user.userName}</h2>
                    <br/><br/><br/><br/>
                </div>    
                
                :
                <div>
                    <div className="right rotate" id="sent">
                        <h1>Enter ingredients</h1>
                        <h1 className="center"> Get Recipes </h1>
                    </div>
                    <img className="responsive-img" id="fridge_img" alt="kitchen" src={require('../food_fridge.png')}></img>
                </div>
            }
                
            </div>
                <div className="bottom_content">
                    <BottomContent />
                </div>
            </div>
            
        )
    }
}

export default Home