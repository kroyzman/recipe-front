import React, { Component } from 'react';
import './ReciepContent.css';
import RecipeRating from './RecipeRating'

class ReciepContent extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            recipe: null,
            avgRating: 0
        }

        this.getReciepById = this.getReciepById.bind(this);
        this.fetchGetReciepResult = this.fetchGetReciepResult.bind(this);
        this.updateAvgRating = this.updateAvgRating.bind(this);
        this.fetchCountViews = this.fetchCountViews.bind(this);
    }

    componentDidMount(){
        const id = this.props.match.params.recipe_id;
        
        this.getReciepById(id)

        
    }

    fetchCountViews(){
        let url = 'http://localhost:8080/recipeforu/count_views';
        console.log( this.state.recipe.id)
        fetch(url + "/" + this.props.user.email + "/" + this.state.recipe.id, {
            method: 'GET',
            headers:{
                'Content-Type': 'application/json'
            }
            }).then(response => {return response.json();})
            .catch(error => console.error('Error:', error));
    }

    getReciepById(id){
        this.fetchGetReciepResult(id, (ReciepByIdResult) => {
            console.log(ReciepByIdResult)
            this.setState({
                recipe: ReciepByIdResult,
                avgRating: ReciepByIdResult.avgRating
            })

            this.fetchCountViews();
          })

    }

    fetchGetReciepResult(id, callback){
        let url = 'http://localhost:8080/recipeforu/reciep';
        console.log(id + " recipe id")
        fetch(url + "/" + id, {
            method: 'GET',
            headers:{
                'Content-Type': 'application/json'
            }
            }).then(response => {return response.json();})
            .then(response => {return callback(response)})
            .catch(error => console.error('Error:', error));
    }

    updateAvgRating(rating){
        this.setState({
            avgRating: rating
        })
    }

    render() { 
        console.log(this.state.recipe)
        return <div className='reciep-content'>
                    {this.state.recipe
                    ?
                        <div className="container center">
                            <h3 id="title" className="title">{this.state.recipe.title}</h3>
                            <img id="recipe-img" src={this.state.recipe.img}/>
                    
                                
                            
                            <span className="info right">
                                
                                <ul className="collection with-header">
                                <li class="collection-header"><h3 className="title">Info</h3></li>
                                    <li className="collection-item"><i class="material-icons info">fastfood</i>Course: {this.state.recipe.instruction.course}</li>
                                    <li className="collection-item"><i class="material-icons info">room_service</i>Cuisine: {this.state.recipe.instruction.cuisine}</li>
                                    <li className="collection-item"><i class="material-icons info">people</i>Servings: {this.state.recipe.instruction.numberOfServings}</li>
                                    <li className="collection-item"><i class="material-icons info">timer</i>Prep Time: {this.state.recipe.instruction.prepTimeInSeconds / 60} Minutes</li>
                                    <li className="collection-item"><i class="material-icons info">star</i>Avg Rating: {this.state.avgRating.toFixed(2)} </li>
                                </ul>
                                <RecipeRating user={this.props.user} recipe={this.state.recipe} updateAvgRating={this.updateAvgRating}/>
                            </span>

                            <div className="ingridients-info">
                            <ul className="collection with-header">
                                <li class="collection-header"><h3 className="title">ingredients</h3></li>
                                {this.state.recipe.instruction.steps.map(ingridient => {
                                    return  <li className="collection-item">{ingridient}</li>
                                })}
                        
                                </ul>
                            </div>
                        </div>

                        
                    :   
                        null    
                    }
                </div>
          
    }
}

export default ReciepContent;