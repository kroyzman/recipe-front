import React, { Component } from "react";
import './SearchReciepRadioButtons.css';

class SearchReciepRadioButtons extends Component {
    constructor(props){
        super(props);
        this.state = {
            option: "option2"
        }
        this.handleOptionChange = this.handleOptionChange.bind(this);
        this.handleCatagory = this.handleCatagory.bind(this);
    }

    handleOptionChange(changeEvent){
        
        this.setState({
            option: changeEvent.currentTarget.value
          });
          if(this.state.option === "option1"){
            this.props.withIngridients(false);
          } else {
            this.props.withIngridients(true);
          }
          
    }

    handleCatagory(e){
        this.props.catagory(e.target.value)
    }

    
    render() {
        return (
            <div className="center font">
                <form>
                    
                    <label className="radio-buttons-reciep">
                        <input className="with-gap" name="group1" value="option1" type="radio" checked={this.state.option === "option1"} onChange={this.handleOptionChange} />
                        <span>With My Ingredients</span>
                    </label>
                    
                    <label className="radio-buttons-reciep">
                        <input className="with-gap" name="group1" value="option2" checked={this.state.option === "option2"} onChange={this.handleOptionChange} type="radio" />
                        <span>Without My Ingredients</span>
                    </label>
                
                </form>
                <div className="divider"></div>
                {this.state.option === "option2"
                    ?
                    <form>
                    <p>
                        <label className="">
                            <input type="checkbox" className="catagories filled-in" value="Main_Dishes" onChange={this.handleCatagory} />
                            <span>Main Dishes</span>
                        </label>

                        <label className="catagories">
                            <input type="checkbox" className="filled-in catagories" value="Desserts" onChange={this.handleCatagory} />
                            <span>Desserts</span>
                        </label>

                        <label className="catagories">
                            <input type="checkbox" className="filled-in catagories" value="Side_Dishes" onChange={this.handleCatagory} />
                            <span>Side Dishes</span>
                        </label>

                        <label className="catagories">
                            <input type="checkbox" className="filled-in catagories" value="Lunch" onChange={this.handleCatagory} />
                            <span>Lunch</span>
                        </label>

                        <label className="catagories">
                            <input type="checkbox" className="filled-in catagories" value="Appetizers" onChange={this.handleCatagory} />
                            <span>Appetizers</span>
                        </label>

                        <label className="catagories">
                            <input type="checkbox" className="filled-in catagories" value="Salads" onChange={this.handleCatagory} />
                            <span>Salads</span>
                        </label>
                        </p>
                        <label className="catagories">
                            <input type="checkbox" className="filled-in catagories" value="Breads" onChange={this.handleCatagory} />
                            <span>Breads</span>
                        </label>

                        <label className="catagories">
                            <input type="checkbox" className="filled-in catagories" value="Breakfast_and_Brunch" onChange={this.handleCatagory} />
                            <span>Breakfast and Brunch</span>
                        </label>

                        <label className="catagories">
                            <input type="checkbox" className="filled-in catagories" value="Soups" onChange={this.handleCatagory} />
                            <span>Soups</span>
                        </label>

                        <label className="catagories">
                            <input type="checkbox" className="filled-in catagories" value="Beverages" onChange={this.handleCatagory} />
                            <span>Beverages</span>
                        </label>

                        <label className="catagories">
                            <input type="checkbox" className="filled-in catagories" value="Condiments_and_Sauces" onChange={this.handleCatagory} />
                            <span>Condiments and Sauces</span>
                        </label>

                        <label className="catagories">
                            <input type="checkbox" className="filled-in catagories" value="Cocktails" onChange={this.handleCatagory} />
                            <span>Cocktails</span>
                        </label>

                        <label className="catagories">
                            <input type="checkbox" className="filled-in catagories" value="KidFriendly" onChange={this.handleCatagory} />
                            <span>Kid Friendly</span>
                        </label>
                    
                </form>
                    :
                    null
                }

                
            </div>
            
        );
    }
}

export default SearchReciepRadioButtons;