import React from 'react'
import './BottomContent.css';

const BottomContent = () => {
    return (
        <div className="container pink darken-3 bottom">
            <div className="container ">
                <h3 className="title center white-text about">About</h3>
                <p className="title center white-text about">
                    "RecipeForU is a recipe search engine that finds recipes you can make with the ingredients you currently have at home. 
                    RecipeForU has indexed hundreds of thousands of recipes, so no matter what ingredients you have, RecipeForU has you covered. 
                    For best results, make sure to tell RecipeForU about every ingredient you have at home. The more ingredients you add to RecipeForU, the better the recipes will be! 
                    RecipeForU is also a practical way to save money. Take full advantage of ingredients you already have, and naturally buy less groceries. ”</p>
            </div>
        </div>
    )
}

export default BottomContent