import React, { Component } from 'react';
import { Link, NavLink} from 'react-router-dom';
import './RecommendedRecipeViewer.css';

class RecommendedRecipeViewer extends React.Component {
    constructor(props){
        super(props)
    }

    render() {
        let {recommendedRecipes} = this.props;

        return (
            <div className="center">
                <div className="row">
                    <ul className='recieps-list'>
                        { recommendedRecipes
                                ?
                                recommendedRecipes.map(reciep => {
                                    return   (
                                                <div className="col s3">
                                                    <div className="container-img">
                                                        <img src={reciep.img} alt="Avatar" className="image_recipe"/>
                                                        <div className="overlay">
                                                        <div>{reciep.title}</div>
                                                        <p><Link to={"/recipe/" + reciep.id} className="btn btnClr">Go To Recipe</Link></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                })
                                :
                                null
                            }
                    </ul>
                </div>
            </div>
            
        )
    }
}

export default RecommendedRecipeViewer