import React, { Component } from "react";

class MyIngridients extends Component {
    constructor(props){
        super(props)
        if(!this.props.loggedIn){
            this.props.history.push('/login')
        }
        this.state = {
            ingridients: this.props
        }
    }

    removeIngridientFromUser(ingridient){
        this.forceUpdate();
        this.props.removeIngridientFromUser(ingridient)
    }

    render() {
        let {ingridients} = this.state.ingridients;

        return (
            <div className="container">
                <ul className='collection with-header col'>
                <li className="collection-header title center"><h4>My Ingredients</h4></li>
                       { ingridients
                            ?
                            ingridients.map(ingridient => {
                                return <li key={ingridient.id} className="collection-item font">{ingridient.name.charAt(0).toUpperCase() + ingridient.name.slice(1)}<i className="right material-icons" onClick={() => this.removeIngridientFromUser(ingridient)}>delete</i></li>   
                            })
                            :
                            null
                        }
                </ul>
            </div>
        );
    }
}

export default MyIngridients;