import React, { Component } from 'react';

class IngridientItem extends React.Component {
    
    constructor(props){
        super(props)
        this.state = {
            ingridientChecked: false
        }

        this.addRemoveIngridientToUser = this.addRemoveIngridientToUser.bind(this);
        this.checkForUserIngridients = this.checkForUserIngridients.bind(this);
    }

    componentDidMount(){
        this.checkForUserIngridients();
    }

    addRemoveIngridientToUser(ingridient){
        if(!this.state.ingridientChecked){
            this.props.addIngridientToUser(ingridient)
            this.setState({
                ingridientChecked : true
            })
        } else {
            this.props.removeIngridientFromUser(ingridient)
            this.setState({
                ingridientChecked : false
            })
        }
    }

    checkForUserIngridients(){
        for(let i = 0 ; i < this.props.userIngridients.length ; i++){
            if(this.props.userIngridients[i].id == this.props.ingridient.id){
                this.setState({
                    ingridientChecked: true
                })
                return true;
            }
        }

        return false;
    }

    render() {
        let {ingridient} = this.props

        return (
            <div className='container ingridient-item'>
                {/* <button className='ingridient-btn' key={ingridient.id} onClick={(() => this.handleClick(ingridient.id)) }>Add Ingridient</button> */}
                <p>
                    {this.state.ingridientChecked
                    ?
                    <label>
                        <input type="checkbox" className="catagories filled-in" onChange={() => this.addRemoveIngridientToUser(ingridient)} checked="checked"/>
                        <span className="font">{ ingridient.name.charAt(0).toUpperCase() + ingridient.name.slice(1) }</span>
                    </label>
                    :
                    <label>
                        <input type="checkbox" className="catagories filled-in" onChange={() => this.addRemoveIngridientToUser(ingridient)}/>
                        <span className="font">{ ingridient.name.charAt(0).toUpperCase() + ingridient.name.slice(1) }</span>
                    </label>
                    }
                    
                </p>
            </div>
            
        )
    }
}

export default IngridientItem