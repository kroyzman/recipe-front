import React, { Component } from 'react';
import './Login.css';

class Login extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            email: "",
            password: ""
        }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.fetchLoginUserResult = this.fetchLoginUserResult.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
    }

    handleInputChange(event){
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
        [name]: value
        });
    }

    handleSubmit(event){
        event.preventDefault()
        this.props.history.push('/')
        this.fetchLoginUserResult((user) => {
            if(user.email){
                this.props.onLogin(user)
            } else {
                console.log("no!")
            }
            
          })
    }

    fetchLoginUserResult(callback){
        let url = 'http://localhost:8080/recipeforu/users/login' + "/" + this.state.email + "/" + this.state.password;
        console.log(url)
        fetch(url, {
        method: 'GET',
        headers:{
            'Content-Type': 'application/json'
        }
        }).then(response => {return response.json();})
            .then(response => {return callback(response)})
            .catch(error => {return console.log(error)});
    }

    render() {
        return (
            <div id="login-div">
            <div className="container-login">
                <h3 className="title">Login</h3>
                <div className="row">
                    <form className="col s12" id="reg-form" onSubmit={this.handleSubmit}>
                        <div className="row">
                            <div className="input-field col s12">
                                <input id="email" name="email" type="email" className="validate" onChange={(e) => this.handleInputChange(e)} required/>
                                <label for="email">Email</label>
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field col s12">
                                <input id="password" name="password" type="password" className="validate" minlength="6" onChange={(e) => this.handleInputChange(e)} required/>
                                <label for="password">Password</label>
                            </div>
                        </div>

                        <div className="center">
                            <button className="btn btn-large btn-register waves-effect waves-light btnClr" type="submit" name="action">Login
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        )
    }

}

export default Login