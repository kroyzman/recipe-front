import React, { Component } from 'react';
import './Signup.css';

class Signup extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            userName: "",
            email: "",
            password: ""
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event){
        event.preventDefault()
        this.props.history.push('/')
        this.fetchSignupUserResult((user) => {
            this.props.onUserSignup(user)
          })
    }

    handleErrors(response) {
        if (!response.ok) {
            console.log(response)
            throw Error(response.statusText);
        }
        return response;
    }

    fetchSignupUserResult(callback){
        let url = 'http://localhost:8080/recipeforu/users/add';
  
        fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: this.state.email,
                userName: this.state.userName,
                password: this.state.password
                
            })
        }).then(this.handleErrors)
        .then(response => {return response.json();})
        .then(response => {return callback(response)})
        .catch(error => console.error('Error:', error));
    }

    handleInputChange(event){
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
        [name]: value
        });
    }

    render() {
        return (
            <div id="signup-div">
                <div className="container-signup">
                    <h3 className="title">Signup</h3>
                    <div className="row">
                        <form className="col s12" id="reg-form" onSubmit={this.handleSubmit}>
                            <div className="row">
                                <div className="input-field col s6">
                                    <input id="user_name" name="userName" type="text" className="validate" onChange={(e) => this.handleInputChange(e)} required/>
                                    <label for="user_name">User Name</label>
                                </div>
                            </div>
                            <div className="row">
                                <div className="input-field col s12">
                                    <input id="email" type="email" name="email" className="validate" onChange={(e) => this.handleInputChange(e)} required/>
                                    <label for="email">Email</label>
                                </div>
                            </div>
                            <div className="row">
                                <div className="input-field col s12">
                                    <input id="password" type="password" name="password" className="validate" minlength="6" onChange={(e) => this.handleInputChange(e)} required/>
                                    <label for="password">Password</label>
                                </div>
                            </div>
    
                            <div className="center">
                                <button className="btn btn-large btn-register waves-effect waves-light btnClr" type="submit" name="action">Register
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
   

export default Signup