import React, { Component } from "react";
import IngridientViewer from './IngridientViewer'

class SearchIngridientByTyping extends Component {
    constructor(props){
        super(props);
        this.state = {
            searchQuery: "",
            ingridients: []
        }

        this.searchInputChange = this.searchInputChange.bind(this);
        this.fetchSearchIngridientResult = this.fetchSearchIngridientResult.bind(this);
        this.searchIngridientClick = this.searchIngridientClick.bind(this);
    }

    searchInputChange(e){
        let searchQuery = e.target.value;
  
        this.setState({
          searchQuery: searchQuery
        })
    }

    searchIngridientClick(){
        this.fetchSearchIngridientResult(this.state.searchQuery ,(ingridients) => {
            this.setState({
                ingridients: ingridients
              })
              console.log(ingridients)
            
          })
    }

    fetchSearchIngridientResult(searchQuery, callback){
        let url = 'http://localhost:8080/recipeforu/ingridients';
        console.log(searchQuery + " ingridient for search...")
        
        fetch(url + "/" + searchQuery, {
          method: 'GET',
          headers:{
            'Content-Type': 'application/json'
        }
        }).then(response => {return response.json();})
          .then(response => {return callback(response)})
          .catch(error => console.error('Error:', error));
        }
        
    render() {
        return (
                <div className="container">
                    <div>
                         <input type='text' placeholder="Ingridient" onChange={ (e) => this.searchInputChange(e) }/>
                    </div>
                    <div className='btn btnClr' onClick={ this.searchIngridientClick }>
                    Search
                    </div>
                    <IngridientViewer userIngridients={this.props.userIngridients} ingridients={this.state.ingridients} removeIngridientFromUser={this.props.removeIngridientFromUser} addIngridientToUser={this.props.addIngridientToUser}/>
                </div>
           
            
        );
    }
}

export default SearchIngridientByTyping;