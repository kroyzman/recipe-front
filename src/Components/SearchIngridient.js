import React, { Component } from "react";
import RadioButtons from './SearchIngridientRadioButtons';

class SearchIngridient extends Component {
    constructor(props){
        super(props)
        if(!this.props.loggedIn){
            this.props.history.push('/login')
        }
    }
    render() {
        return (
            <div>
                <div className="container">
                    <RadioButtons userIngridients={this.props.userIngridients} addIngridientToUser={this.props.addIngridientToUser} removeIngridientFromUser={this.props.removeIngridientFromUser}/>
                </div>
            </div>
        );
    }
}

export default SearchIngridient;