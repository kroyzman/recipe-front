import React, { Component } from 'react';
import { Link, NavLink} from 'react-router-dom';

class ReciepViewer extends React.Component {
    constructor(props){
        super(props)
    }

    removeFavouriteRecipe(recipe){
        this.forceUpdate();
        this.props.removeFavouriteRecipe(recipe)
    }

    handleFavouriteRecipe(recipe){
        this.forceUpdate();
        this.props.handleFavouriteRecipe(recipe)
    }

    render() {
        let {recieps} = this.props;

        return (
            <div className="center">
                <div className="row">
                    <ul className='recieps-list'>
                        { recieps
                                ?
                                recieps.map(reciep => {
                                    return   (
                                                <div className="col s12 m4">
                                                    <div className="card">
                                                        <div className="card-image">
                                                            <img src={reciep.img}/>
                                                            <span className="card-title">{reciep.title}</span>
                                                            {this.props.checkForFavouriteRecipe(reciep)
                                                            ?
                                                                <a className="btn-floating halfway-fab waves-effect waves-light red" onClick={() => this.removeFavouriteRecipe(reciep)}><i className="material-icons">remove_circle_outline</i></a>
                                                            :
                                                                <a className="btn-floating halfway-fab waves-effect waves-light red" onClick={() => this.handleFavouriteRecipe(reciep)}><i className="material-icons">favorite</i></a>
                                                            }
                                                            
                                                        </div>
                                                        <div className="card-content">
                                                            <p className="pa">
                                                                <div>Course: {reciep.instruction.course}</div>
                                                                <div>Cuisine: {reciep.instruction.cuisine}</div>
                                                                <div>Prep Time: {reciep.instruction.prepTimeInSeconds / 60} Minutes</div>
                                                            </p>
                                                        <p><Link to={"/recipe/" + reciep.id} className="btn btnClr recipe">Go To Recipe</Link></p>
                                                        </div>
                                                    </div>
                                                    <span className="font color_font">{reciep.title}</span>
                                                </div>
                                            )
                                })
                                :
                                null
                            }
                    </ul>
                </div>
            </div>
            
        )
    }
}

export default ReciepViewer